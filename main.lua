#!/bin/env lua

package.path = package.path .. ';factorio-data/core/?.lua;factorio-data/base/?.lua;factorio-data/core/lualib/?.lua;factorio-data/core/lualib/?/?.lua;;'
local serpent = require('serpent')

defines = require('defines')
require('util')
require('dataloader')

require('factorio-data/core/data')
require('factorio-data/base/data')


local file = io.open("data.raw", "w+")
io.output(file)

io.write(serpent.block(data.raw, {comment = false}))

io.close(file)
